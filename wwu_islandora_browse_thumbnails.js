// https://bitbucket.org/wwulibraries/wwu_islandora_browse_thumbnails

// when the DOM is ready, 
jQuery( document ).ready(function() {
    // find the current pager number (assuming the View has Ajax OFF and Paging enabled)
    var currentPageNumber = jQuery('.pager-current').text() - 1;

    // for each link in the Browse Thumbnails View
    jQuery("a.wwu-browse-thumbnails-view-details-link").each(function() {
        var theLink = jQuery(this);
        var _href = theLink.attr("href"); 
        // and then add that page number to the querystring
        jQuery(this).attr("href", _href + '?page=' + currentPageNumber);
    });

    // now add the PID (if it exists in the URL) to the pager links
    // if we don't do this, we can end-up with an old PID because of how the pager links are generated
    var  path_parts = window.location.pathname.split('/');
    var  PID = path_parts.pop();      // the last element  (wwu:12345)
    if (PID.includes(":")) {
        // for each link in the pager, replace an old PID with the current PID
        jQuery("ul.pager li a").each(function() {
            var pageNumber = "page=0";
            var _href = jQuery(this).attr("href"); 
            if (_href.includes("?")) {
                var  pageNumber = _href.split('?').pop();
            }
            //  use the current path and append the pager number to it
            var newHref = window.location.pathname + '?' + pageNumber
            jQuery(this).attr("href", newHref);
        })
    }    
})
