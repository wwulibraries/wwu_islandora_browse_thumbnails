# Overview
This Drupal 7 module makes it possible to connect two Views in the Page Manager.  For instance, this D7 Page ( https://mabel.wwu.edu/browse/thumbnails ) uses a "Browse" View and a "View Details" View to show the details of the selected thumbnail.

This can be setup by creating two Views:

- "Browse Thumbnails"
  
    - add a rewrite rule to the thumbnail with a className (found in the .js file):
  
       `<div style="max-height:100px; overflow: hidden"><a class="wwu-browse-thumbnails-view-details-link" href="/browse/thumbnails/[PID]" alt="[dc.title]" title="[dc.title]">[tn_cache]</a></div>`

- "View Details"

The "Browse" View has Ajax enabled, and "View Details" View has a Filter that looks for the PID (Islandora identifier) in the URL to show the details of that record.  This module uses jQuery to append the page number to the querystring to make it possible to preserve the page number.

# Requirements
- Drupal 7.x
- Islandora 7.x

# Installation
- navigate to your modules directory (usually /var/www/drupal/sites/all/modules/)
- clone this repository into that folder (e.g, 
git clone https://bitbucket.org/wwulibraries/wwu_islandora_browse_thumbnails.git)
- drush en wwu_islandora_browse_thumbnails

# Author: 
David Bass @ WWU

# License: 
MIT